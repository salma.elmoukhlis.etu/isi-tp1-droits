#!/bin/bash

#prepare environement de test:
su - admin_server_fichier <<!
adminserverfichier
cd /home/dir_c
touch fileInDirC.txt
mkdir dirInDir_c
cd /home/dir_c/dirInDir_c
touch file.txt
!

su - lambda_aa <<!
lambda_aa
cd /home/dir_a/
touch fileBelongingToLambda_aa
!

su - lambda_a <<!
lambda_a

cd /home/dir_a 
touch test.txt
mkdir dirInDir_a
cd /home/dir_a/dirInDir_a
touch file.txt
#end

#start tests
echo "************** STARTING TESTS **************"
echo -n "Test with user :"; whoami 

echo "--------------------------------------------------------------"

echo "Test 1: lecture des fichiers et sous-répertoires contenus dans dir_a et dir_c"
cd /home/dir_a/
echo -n "pwd = "; pwd
echo "cmd de test : ls -la"
ls -la
echo "cmd de test : cd dirInDir_a && ls -la"
cd /home/dir_a/dirInDir_a/
echo -n "pwd = "; pwd
ls -la
cd /home/dir_c/
echo -n "pwd = "; pwd
echo "cmd de test : ls -la"
ls -la
echo "cmd de test : cd dirInDir_c && ls -la"
cd /home/dir_c/dirInDir_c
echo -n "pwd = "; pwd
ls -la
cd ..

echo "--------------------------------------------------------------"

echo "Test 2: ne peut pas modifier, renommer, effacer les fichiers dans dir_c, ou créer de nouveaux fichiers dans dir_c"
echo "cmd de test : \"Hello world\" > fileInDirC.txt"
echo "Hello world" > fileInDirC.txt
echo "cmd de test : mv fileInDirC.txt myFile"
mv fileInDirC.txt myFile
echo "cmd de test : rm fileInDirC"
rm fileInDirC.txt
echo "cmd de test : touch myNewFile"
touch myNewFile

echo "--------------------------------------------------------------"

echo "Test 3: modification de tous les fichiers contenus dans l’arborescence à partir de dir_a, et création de nouveaux fichiers et répertoires dans dir_a"
cd /home/dir_a/
echo -n "pwd = "; pwd
echo "cmd de test : \"Hello world\" > test.txt"
echo "Hello world" > test.txt
echo "cmd de test : cat test.txt"
cat test.txt
echo "cmd de test : touch myNewFile"
touch myNewFile
echo "cmd de test : mkdir myNewDir"
mkdir myNewDir
ls -la

echo "--------------------------------------------------------------"

echo "Test 4: pas le droit d’effacer, ni de renommer, des fichiers dans dir_a qui ne leur appartiennent pas"
ls -ls
echo "cmd de test : rm fileBelongingToLambda_aa"
rm fileBelongingToLambda_aa
echo "cmd de test : mv fileBelongingToLambda_aa myFileLambda_a"
mv fileBelongingToLambda_aa myFileLambda_a

echo "--------------------------------------------------------------"

echo "Test 5: ne peut pas lire, modifier, effacer les fichiers dans dir_b, et ne peut pas créer de nouveaux fichiers dans dir_b"
cd /home/dir_b/
echo -n "pwd = "; pwd
echo "cmd de test : ls -la"
ls -la
echo "cmd de test : \"Hello world\" > test.txt"
echo "Hello world" > test.txt
echo "cmd de test : rm test.txt"
rm test.txt
echo "cmd de test : touch myNewFile"
touch myNewFile
echo "************** END TESTS **************"

#remove created files and directories
cd /home/dir_a/
rm test.txt
rm -r dirInDir_a
rm myNewFile
rm -r myNewDir
!

su - admin_server_fichier <<!
adminserverfichier
cd /home/dir_c
rm fileInDirC.txt
rm -r dirInDir_c
!

su - lambda_aa <<!
lambda_aa
cd /home/dir_a
rm fileBelongingToLambda_aa
!






