# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Ait Kheddache , Nouria, email:nouria.aitkheddche.etu@univ-lille.fr 

- ELMOUKHLIS, Salma, email:salma.elmoukhlis.etu@univ-lille.fr
## Question 1

Le processus ne peut pas écrire dans le fichier car l'utilisateur toto n'a pas les droits en écriture sur le fichier.

## Question 2

Pour un répertoire ça signifie qu'on a accès aux sous-répertoires dans le répertoire donc c'est possible de naviguer dans ce répertoire, d'entrer dans sa hiérarchie.

Si on enlève le droit d'execution au répertoire mydir au groupe ubuntu, l'utilisateur toto ne peut pas accéder au dossier mydir, car le groupe auquel il appartient n'a pas les droits d'execution dessus et toto n'est pas propriétaire du fichier.

toto peut lister les éléments de mydir, mais ne peut pas voir leurs propriétés. Cela s'explique par le fait que le groupe auquel toto appartient n'a pas les droits d'execution sur le dossier, mais a les droits de lecture.

## Question 3

1000 = ubuntu
1001 = toto

Pour toto:
* EUID et RUID : 1001 
* EGID et RGID : 1000
Le processus n'arrive pas à ouvrir le fichier en lecture

Avec le flag set-user-id:
* EUID, EGID et RGID: 1000
* RUID: 1001
Le processus peut ouvrir le fichier en lecture.



## Question 4

Avec le flag set-user-id et l'utilisateur toto:
* EUID: 1001
* EGID: 1000

set_user_id permet au fichier executable de s'octroyer les droits du propriétaire pour réaliser temporairement toutes les opérations que le fichier comporte. Sans le set_user_id, le fichier executerait ce qu'il contient avec les droits de l'utilisateur. Par conséquent, il ne pourrait pas tout executer.

Un utilisateur peut changer un attribut du fichier /etc/passwd sans demander à l'administrateur en passant par un programme qui le fait à sa place et ayant le flag set-user-id.


## Question 5

La commande `chfn` permet de modifier les informations d'un utilisateur dans le fichiet /etc/passwd 

       toto@etudiant-Latitude-3410:/home/users/etudiant$ ls -al /usr/bin/chfn 
           -rwsr-xr-x 1 root root 76496 mars  19  2020 /usr/bin/chfn

	
Ce fichier appartient à l'administrateur qui y a tout les droits. Les autres utilisateurs peuvent le lire et l'exécuter, mais pas le modifier. Il porte le flag set-user-id, donc n'importe quel utilisateur qui l'exécute bénéficiera d'accès privilégiés pour opérer sur les fichiers qu'il utilise.

Les informations du fichier /etc/passwd sont bien mises à jour lors de l'exécution de chfn par toto.


## Question 6

Les mots de passe sont stockés  dans le fichier /etc/shadow qui n'est lisible que par root. Cela évite de les stocker dans /etc/passwd qui est accessible à tous le monde.

## Question 7

Mettre les scripts bash dans le repertoire *question7*.
Il existe plusieurs solutions. L'une d'entres-elles est la suivante : 

Créer 3 groupes et 3 utilisateurs : 
- group_a : lambda_a, admin 
- group_b : lambda_b, admin 
- group_ab : lambda_a, lambda_b, admin 



l'autre solution:
commands: 
adduser lambda_a 
adduser lambda_b 
addgroup groupe_a 
addgroup groupe_b 
adduser admin 
adduser lambda_a groupe_a 
adduser lambda_b groupe_b 
adduser admin_server_fichier groupe_b 
adduser admin_server_fichier groupe_a 
su admin 
admin$ mkdir dir_a dir_b dir_c 
admin$ chgrp dir_a groupe_a 
admin$ chgrp dir_b groupe_b 
admin$ chmod g+s dir_a 
admin$ chmod g+s dir_b 
admin$ chmod o-r dir_a 
admin$ chmod o-r dir_a 
admin$ chmod +t dir_a 
admin$ chmod +t dir_b 

chmod g+s pour que les sous-répertoires et fichiers créés dans les dir_a et dir_b appartiennent au même groupe groupe_a pour les sous-répertoires de dir_a et groupe_b pour ceux de dir_b.

chmod +t pour dir_a et dir_b pour que les utilisateurs ne puissent pas supprimer ou renommer des fichiers dont ils ne sont pas propriétaires tout en pouvant quand même lire, modifier ou créer des fichier dans ces répertoires. 


## Question 8

Le programme dans le repertoire *question8*.

## Question 9

Le programme dans le repertoire*question9*.







