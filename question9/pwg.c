#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>

int main(int args, char*argv[]) {
	if(args > 1) { perror("Vous avez rentré trop d'arguments"); exit(1); }
	FILE * fp; FILE * fnew;
	fp = fopen("/home/admin/passwd.txt", "r");
	fnew = fopen("/home/admin/passwdNew.txt", "w+");
	if(!fp) { perror("Erreur lors de l'ouverture du fichier passwd"); exit(1); }
	if(!fnew) { perror("Erreur lors de l'ouverture du fichier passwdnew"); exit(1); }
	char ligne[150];
	char idUserChar[10];
	sprintf(idUserChar, "%d", getuid());
	int boolean = 0;
	while(fgets(ligne, 150, fp)) {
		if(strstr(ligne,idUserChar)) { 
			printf("Saississez votre mot de passe :\n");
			char mdpAncien[40] = {0};
			scanf("%s", mdpAncien);
			if(strstr(ligne, mdpAncien)) {
			  //Si l'utilisateur a entrée son ancien mdp
			  printf("Saississez votre nouveau mot de passe :\n");
			  char mdpNew[40] = {0};
			  scanf("%s", mdpNew);
			  fprintf(fnew, "%s %s\n", idUserChar, mdpNew);
			  boolean = 1;
			} else {
			  //SI l'utilisateur n'a pas entrée le bon mot de passe
			  printf("Mauvais mot de passe \n");
			  remove("/home/admin/passwdNew.txt");
			  fclose(fp);
			  fclose(fnew);
			  exit(1);
			}
		} else {
		  //Si la ligne ne correspond pas à l'utilisateur
		   fprintf(fnew, "%s", ligne);
		}
	}
	if(boolean == 0) {
	  //Si l'utilisateur n'a jamais eu de mot de passe (donc pas dans passwd)
	  printf("Saississez votre nouveau mot de passe :\n");
	  char mdpNew[40] = {0};
	  scanf("%s", mdpNew);
	  fprintf(fnew, "%s %s\n", idUserChar, mdpNew);
	}
	remove("/home/admin/passwd.txt");
	rename("/home/admin/passwdNew.txt", "/home/admin/passwd.txt");
	fclose(fnew);
	fclose(fp);
	return 0;
}
