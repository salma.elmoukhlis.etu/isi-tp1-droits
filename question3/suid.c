#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


int main(int argc, char *argv[])
{
    FILE *f;
    printf("EUID: %d\n", geteuid());
    printf("EGID: %d\n", getegid());
    printf("RUID: %d\n", getuid());
    printf("RGID: %d\n", getgid());
    f = fopen("mydir/data.txt", "r");
    if(f == NULL){
	    printf("Cannot open file \n");
	    exit(0);
    }
      
    printf("File opens correctly\n");
    char c;
    c = fgetc(f);
    while (c != EOF){
	    printf("%c", c);
	    c = fgetc(f);
    }
    fclose(f);
  
