#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>

int main(int args, char*argv[]) {

	if(args < 2) { perror("Veuillez entrer le nom du fichier"); exit(1); }
	if(args > 2) { perror("Vous avez rentré trop d'arguments"); exit(1); }
	if(access(argv[1], F_OK) != 0) { perror("Veuillez entrer le nom d'un fichier existant"); exit(1); }
	struct stat fichier;
	if(stat(argv[1], &fichier) == -1) {  perror("Erreur lors de la récupération des données du fichier"); exit(1); }
	int gidUser = getgid();
	int gidFile = fichier.st_gid;
	if(gidUser != gidFile) { perror("Vous ne pouvez pas supprimer ce fichier"); exit(1); }
	printf("Saississez votre mot de passe :\n");
	char mdp[40] = {0};
	scanf("%s", mdp);
	
	FILE * fp;
	fp = fopen("/home/admin/passwd", "r");
	if(!fp) { perror("Erreur lors de l'ouverture du fichier passwd"); exit(1); }
	char ligne[150];
	int idUser = getuid();
	char idUserChar[150];
	sprintf(idUserChar, "%d", idUser);
	printf("Mon idUser : %s et mon mdp : %s\n", idUserChar, mdp);
	while(fgets(ligne, 150, fp)) {
		printf("%s",ligne);
		if(strstr(ligne,idUserChar) && strstr(ligne, mdp)) {
			printf("Utilisateur et MDP correspondent\n");
			remove(argv[1]);
			fclose(fp);
            printf("Fichier %s supprimer", argv[1]);
			exit(0);	
		}
	}
	fclose(fp);
	perror("le passe est incorrect");
	return 0;
}
